package com.xdtech.web.model;

import java.util.HashMap;
import java.util.Map;

import javassist.expr.NewArray;

/**
 * $.ajax后需要接受的JSON
 * 
 * @author
 * 
 */
public class ResultMessage {

	private boolean success = true;// 是否成功
	private String msg = "操作成功";// 提示信息
	private Object obj = null;// 其他信息
	private Map<String, Object> attributes = new HashMap<String, Object>();// 其他参数
	
	
	
	public ResultMessage() {
		super();
	}

	
	public ResultMessage(boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}


	public ResultMessage(Object obj) {
		super();
		this.obj = obj;
	}


	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public boolean isSuccess() {
		return success;
	}

	public ResultMessage setSuccess(boolean success) {
		if (success) {
			this.msg = "操作成功";
		}else {
			this.msg = "操作失败";
		}
		this.success = success;
		return this;
	}
	public void setFailMsg(String failMsg) {
		this.success = false;
		this.msg = failMsg;
	}
	
	public static ResultMessage getFailMessage() {
		return new ResultMessage(false,"操作失败");
	}

}
