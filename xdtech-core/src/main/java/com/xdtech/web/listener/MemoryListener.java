/*
 * Copyright 2013-2014 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdtech.web.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.xdtech.common.utils.ApplicationContextUtil;
import com.xdtech.core.listener.SysInitListener;

/**
 * 
 * @author max.zheng
 * @create 2014-12-26下午9:11:24
 * @since 1.0
 * @see
 */
public class MemoryListener implements ServletContextListener {

	private Logger log = Logger.getLogger(getClass());
	/**
	 * @description
	 * @author max.zheng
	 * @create 2014-12-26下午9:11:45
	 * @modified by
	 * @param arg0
	 */
	@Override
	public void contextDestroyed(ServletContextEvent contextEvent) {
		System.out.println("application destroying");
	}

	/**
	 * @description
	 * @author max.zheng
	 * @create 2014-12-26下午9:11:45
	 * @modified by
	 * @param arg0
	 */
	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		
		ServletContext application = contextEvent.getServletContext();
		ApplicationContextUtil.setApplication(application);
	    log.info("应用监听器启动,进行初始化......application initing......");
	    SysInitListener sysInitListener = ApplicationContextUtil.getContext().getBean(SysInitListener.class);
	    sysInitListener.initDataToWebApplication();
	    log.info("application initing over......");
	}

}
