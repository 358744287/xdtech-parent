/*
 * Copyright 2013-2014 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdtech.web.freemark.tags;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;

/**
 * 
 * @author max.zheng
 * @create 2014-11-30上午9:28:00
 * @since 1.0
 * @see
 */
public class ComboTreeTag extends EasyUiTag {

	/**
	 * @description
	 * @author max.zheng
	 * @create 2014-10-12下午8:16:23
	 * @modified by
	 * @param env
	 * @param params
	 * @param body
	 * @throws IOException
	 * @throws TemplateException
	 */
	@Override
	public void render(Environment env, Map params, TemplateDirectiveBody body)
			throws IOException, TemplateException {
		// <input class="easyui-combotree"
		// data-options="url:'tree_data1.json',method:'get',required:true"
		// style="width:200px;">
		//标签方式
		StringBuffer sb = new StringBuffer();
		String id = params.get("id")==null?UUID.randomUUID().toString():params.get("id").toString();
		String name = params.get("name")==null?"none":params.get("name").toString();
		String width = params.get("width")==null?"159":params.get("width").toString();
//		sb.append("<input id=\""+id+"\" style=\"width:"+width+"px;\" class=\"easyui-combotree\" name=\""+name+"\"")
//		  .append(" "+multiple)
//		  .append(" data-options=\"")
//		  .append(addProperties("url", params.get("url"), true))
//		  .append(addProperties("valueField", params.get("valueField")==null?"value":params.get("valueField"), true))
//		  .append(addProperties("textField", params.get("textField")==null?"name":params.get("textField"), true))
//		  .append("\"/>");
		
		//js方式
//		<script type="text/javascript">
//		$(function() {
//			$('#11111').combotree({
//			    url: 'group.do?usergroupTree&userId=${(userItem.id)!}',
//			    onChange: function (a,b) {
//			    	 $('#test').val($('#11111').combotree('getValues'));
//			    },
//			    required: true,
//			    multiple:true,
//			});
//		});
//	</script>	
//	<input id="11111" >
//	<input id="test" name="groupIds" hidden="true">
		
		//	<select id="11111" name="groupIds" class="easyui-combotree" data-options="url:'group.do?usergroupTree',method:'post'" multiple style="width:159px;"></select> -->
//		<@eu.comboTree id="123" url="group.do?usergroupTree&userId=${(userItem.id)!}" name="groupIds" multiple="multiple"/>
		
		sb.append("<script type=\"text/javascript\">")
		  .append("$(function() {")
		  .append("$('#"+id+"').combotree({")
		  .append(addProperties("url", params.get("url"), true))
		  .append(addProperties("lines", true, false))
		  .append(addProperties("cascadeCheck", params.get("cascadeCheck"),false, false))
		  .append(addProperties("required", params.get("required"),false, false))
		  .append(addProperties("multiple", params.containsKey("multiple") ? params.get("multiple") : true ,false))//此处一直都是多选，为了解决下拉数必须要有checkbox
		  .append(addProperties("disabled", params.get("disabled")==null?"false":params.get("disabled"), false))
		  .append(addEvent(params,id))
		  .append("});")
		  .append("});")
		  .append("</script>");
	   sb.append("<input id=\""+id+"\" name=\""+name+"\" style=\"width:"+width+"px\"/>")
	   	 .append("<input id=\""+id+"hidden\" name=\""+name+"\"  type=\"hidden\" />");
		writeBody(env, sb.toString());
	}
	/**
	 * 添加事件相关操作
	 * @author max.zheng
	 * @create 2014-9-29下午9:23:33
	 * @modified by
	 * @param params
	 * @return
	 */
	private String addEvent(Map params,String id) {
		StringBuffer sb = new StringBuffer();
		sb.append("onChange : function(a, b) {")
		  .append("$('#"+id+"hidden').val( $('#"+id+"').combotree('getValues'));");
		if (params.get("onChange")!=null) {
			sb.append(params.get("onChange")+";");
		}
		sb.append("},");
		if (params.get("onSelect")!=null) {
			sb.append("onSelect: function(data){"+params.get("onSelect")+";},");
		}
//		sb.append("onLoadSuccess: function(node,data){");
//		if (params.get("onLoadSuccess")!=null) {
//			sb.append(params.get("onLoadSuccess")+";");
//		}
//		//设置默认值
//		if(params.get("values")!=null) {
//			sb.append("$('#"+id+"').combotree('setValues',["+params.get("values")+"]);");
//		}
//		sb.append("},");
		//设置单选下拉树情况下，只能选中一个对象控制
		if ("false".equals(params.get("multiple")+"")) {
			//单选情况
			sb.append("onBeforeCheck:function(node,checked){")
			  .append("var t =$('#"+id+"').combotree('tree');")
			  .append("var nodes =t.tree('getChecked');")
			  .append("for (var i = 0; i < nodes.length; i++) {")
			  .append("if(nodes[i].id!=node.id) {")
			  .append(" t.tree('uncheck',nodes[i].target);")
			  .append("}}},");
		}
		
		return sb.toString();
	}

}

