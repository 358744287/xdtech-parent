/*
 * Copyright 2013-2014 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdtech.web.freemark.tags;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;

/**
 * 
 * @author max.zheng
 * @create 2014-10-12下午8:15:59
 * @since 1.0
 * @see
 */
public class ComboBoxTag extends EasyUiTag {

	/**
	 * @description
	 * @author max.zheng
	 * @create 2014-10-12下午8:16:23
	 * @modified by
	 * @param env
	 * @param params
	 * @param body
	 * @throws IOException
	 * @throws TemplateException
	 */
	@Override
	public void render(Environment env, Map params, TemplateDirectiveBody body)
			throws IOException, TemplateException {
//		<input class="easyui-combobox"  name="language" data-options="
//                url:'combobox_data1.json',
//                method:'get',
//                valueField:'id',
//                textField:'text',
//                panelHeight:'auto',
//                icons:[{
//                    iconCls:'icon-add'
//                },{
//                    iconCls:'icon-cut'
//                }]
//        ">
		
		StringBuffer sb = new StringBuffer();
		String id = params.get("id")==null?UUID.randomUUID().toString():params.get("id").toString();
		String name = params.get("name")==null?"none":params.get("name").toString();
		String width = params.get("width")==null?"159":params.get("width").toString();
//		sb.append("<input id=\""+id+"\" style=\"width:"+width+"px;\" class=\"easyui-combobox\" name=\""+name+"\"")
//		  .append(addValidType(params.get("validType"), params.get("invalidMessage")))
//		  .append(" data-options=\"")
////		  .append(addProperties("width", params.get("width")==null?"150":params.get("width"), false))
//		  .append(addProperties("url", params.get("url"), true))
//		  .append(addProperties("required", params.get("required")==null?"false":params.get("required"), false))
//		  .append(addProperties("multiple", params.get("multiple")==null?"false":params.get("multiple"), false))
//		  .append(addProperties("valueField", params.get("valueField")==null?"value":params.get("valueField"), true))
//		  .append(addProperties("textField", params.get("textField")==null?"name":params.get("textField"), true))
//		  .append(addEvent(params.get("onSelect")))
//		  .append("\"/>");
//		writeBody(env, sb.toString()); editable:false
		
		sb.append("<script type=\"text/javascript\">")
		  .append("$(function() {")
		  .append("$('#"+id+"').combobox({")
		  .append(addProperties("url", params.get("url"), true))
		  .append("onChange : function(a, b) {")
		  .append("$('#"+id+"hidden').val( $('#"+id+"').combobox('getValues'));")
		  .append("},")
		  .append(addProperties("url", params.get("url"), true))
		  .append(addProperties("required", params.get("required")==null?"false":params.get("required"), false))
		  .append(addProperties("multiple", params.get("multiple")==null?"false":params.get("multiple"), false))
		  .append(addProperties("valueField", params.get("valueField")==null?"value":params.get("valueField"), true))
		  .append(addProperties("textField", params.get("textField")==null?"name":params.get("textField"), true))
		  .append(addProperties("disabled", params.get("disabled")==null?"false":params.get("disabled"), false))
		  .append(addProperties("editable", params.get("editable")==null?"false":params.get("editable"), false))
		  .append(addEvent(params.get("onSelect")))
		  .append("});")
		  .append("});")
		  .append("</script>");
	   sb.append("<input id=\""+id+"\" name=\""+name+"\" "+addValidType(params.get("validType"), params.get("invalidMessage"))+" style=\"width:"+width+"px\"/>");
	   if (!"true".equals(params.get("disabled")+"")) {
		   //禁用只读就不用hidden，避免表单提交出现name相同，值会val,val情况
		   sb.append("<input id=\""+id+"hidden\" name=\""+name+"\"  type=\"hidden\" />");
	   }
	   	 
		writeBody(env, sb.toString());
		
		
	}

	/**
	 * 
	 * @author <a href="max.zheng@zkteco.com">郑志雄</>
	 * @since 2015-3-18 下午5:37:06
	 * @param object
	 * @return 
	 */
	private String addEvent(Object event)
	{
		if (null != event)
		{
			return "onSelect: function(value){"+event+"},";
		}
		else
		{
			return "";
		}
	}

}
