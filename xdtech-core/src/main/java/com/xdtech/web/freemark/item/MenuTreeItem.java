package com.xdtech.web.freemark.item;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.xdtech.common.fomat.TreeFormat;

public class MenuTreeItem implements Serializable,
		TreeFormat<MenuTreeItem, Long> {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String text;

	private String iconCls;

	private String state;

	private MenuTreeItem parent;

	private List<MenuTreeItem> children = new ArrayList<MenuTreeItem>();
	
	private String url;
	
	private Boolean iframe;

	public MenuTreeItem(Long id) {
		super();
		this.id = id;
	}

	public MenuTreeItem() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addChildren(MenuTreeItem child) {
		if (this.children == null) {
			this.children = new ArrayList<MenuTreeItem>();
		}
		this.children.add(child);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public MenuTreeItem getParent() {
		return parent;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<MenuTreeItem> getChildren() {
		return children;
	}

	public void setChildren(List<MenuTreeItem> children) {
		this.children = children;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setParent(MenuTreeItem parent) {
		this.parent = parent;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getIframe() {
		return iframe;
	}

	public void setIframe(Boolean iframe) {
		this.iframe = iframe;
	}

}
