/*
 * Project Name: xdtech-core
 * File Name: DhtmlxTags.java
 * Copyright: Copyright(C) 1985-2016 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.web.freemark.tags.dx;

import freemarker.template.SimpleHash;

/**
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2016-7-22 上午11:10:31
 */
public class DxTags extends SimpleHash {
	private static final long serialVersionUID = 1L;

	public DxTags() {
		put("grid", new GridTag());
    }
}