/*
 * Project Name: xdtech-core
 * File Name: GridTag.java
 * Copyright: Copyright(C) 1985-2016 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.web.freemark.tags.dx;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.UUID;

import com.xdtech.web.freemark.item.GridColumn;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;

/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2016-7-22 上午11:17:51
 */
public class GridTag extends DhtmlxTag
{

	/*
	 * (非 Javadoc)
	 * render
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @param env
	 * 
	 * @param params
	 * 
	 * @param body
	 * 
	 * @throws IOException
	 * 
	 * @throws TemplateException
	 * 
	 * @see com.xdtech.web.freemark.tags.SecureTag#render(freemarker.core.Environment, java.util.Map, freemarker.template.TemplateDirectiveBody)
	 */
	@Override
	public void render(Environment env, Map params, TemplateDirectiveBody body) throws IOException, TemplateException
	{
		String gridId = params.get("id") == null ? UUID.randomUUID().toString().replaceAll("-", "") : params.get("id").toString();
		String className = params.get("item").toString();
		String url = params.get("url").toString();
		StringBuffer sb = new StringBuffer("<div id=\""+gridId+"recinfoArea\"></div><div id=\""+gridId+"Gridbox\"></div><div id=\""+gridId+"PagingArea\"></div>");
		String htmlGridObj =  "Grid"+gridId;
		sb.append("<script>")
		  .append("var "+htmlGridObj+" = new dhtmlXGridObject('"+gridId+"Gridbox');")
		  .append(htmlGridObj+".setImagePath('dx/codebase/imgs/');")
		  .append(createColumns(htmlGridObj,className))
		  .append(htmlGridObj+".enableAutoHeight(true);")//是否可自动适应高度"
		  .append(htmlGridObj+".enableAutoWidth(true);")//是否可自动适应宽度
		  .append(htmlGridObj+".enableMultiselect(true);")
		  .append(htmlGridObj+".enablePaging(true,30,null,'"+gridId+"PagingArea',true,'"+gridId+"RecinfoArea');")
		  .append(htmlGridObj+".init();")
		  .append(htmlGridObj+".setPagingSkin('toolbar');")
		  .append(htmlGridObj+".load('"+url+"',null,'json');")
		  .append("</script>");
		writeBody(env, sb.toString());
	}

	/**
	 * 
	 * @author <a href="max.zheng@zkteco.com">郑志雄</>
	 * @since 2016-7-22 下午2:36:30
	 * @param className
	 * @return
	 */
	private String createColumns(String htmlGridObj,String className)
	{
		StringBuffer sb = new StringBuffer();
		try
		{
			Class<?> itemCls = Class.forName(className);
			Field[] fields = itemCls.getDeclaredFields();
			StringBuffer header = new StringBuffer("");
			StringBuffer initWidths = new StringBuffer("");
			StringBuffer colAlign = new StringBuffer("");
			StringBuffer colTypes = new StringBuffer("");
			StringBuffer colSorting = new StringBuffer("");
			for (Field field : fields)
			{
				if ("id".equals(field.getName()))
				{
					header.append("<input type='checkbox' id='checkId'/>,");
					initWidths.append("48,");
					colTypes.append("ro,");
//					colSorting.append("int,");
				}
				GridColumn gridColumn = field.getAnnotation(GridColumn.class);
				//新增列表是否显示控制,主要是解决部分情况下表格查询数据不需要直接显示控制 add by max 20151121
				if (gridColumn != null&&gridColumn.show())
				{
					header.append(gridColumn.title()+",");
					initWidths.append(gridColumn.width()==-1?"*,":gridColumn.width()+",");
					colAlign.append("left,");
					colTypes.append("ro,");
//					colSorting.append("int,");
				}
			}
			
			sb.append(htmlGridObj+".setHeader('"+header.substring(0, header.length()-1)+"');");
			sb.append(htmlGridObj+".setInitWidths('"+initWidths.substring(0, initWidths.length()-1)+"');");
			sb.append(htmlGridObj+".setColAlign('"+colAlign.substring(0, colAlign.length()-1)+"');");
			sb.append(htmlGridObj+".setColTypes('"+colTypes.substring(0,colTypes.length()-1)+"');");
//			sb.append(htmlGridObj+".setColSorting('"+colSorting.substring(0, colSorting.length()-1)+"');");
		}
		catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		  .append("myGrid.setHeader('Column A, Column B');")
//		  .append("myGrid.setInitWidths('100,*');")
//		  .append("myGrid.setColAlign('right,left');")
//		  .append("myGrid.setColTypes('ro,ed');")
//		  .append("myGrid.setColSorting('int,str');")
		return sb.toString();
	}
}
