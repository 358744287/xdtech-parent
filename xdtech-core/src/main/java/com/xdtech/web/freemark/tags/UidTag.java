/*
 * Project Name: xdtech-core
 * File Name: UidTag.java
 * Copyright: Copyright(C) 1985-2015 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.web.freemark.tags;

import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;

/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2015-6-4 下午2:44:58
 */
public class UidTag extends EasyUiTag
{

	/* (非 Javadoc)
	 * render
	 * <p>对override方法的补充说明，若不需要则删除此行</p>
	 * 
	 * @param env
	 * @param params
	 * @param body
	 * @throws IOException
	 * @throws TemplateException
	 * @see com.xdtech.web.freemark.tags.SecureTag#render(freemarker.core.Environment, java.util.Map, freemarker.template.TemplateDirectiveBody)
	 */
	@Override
	public void render(Environment env, Map params, TemplateDirectiveBody body) throws IOException, TemplateException
	{
		String id = params.get("id")==null?RandomStringUtils.random(10, "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"):params.get("id").toString();
		writeBody(env, id);
	}

}
