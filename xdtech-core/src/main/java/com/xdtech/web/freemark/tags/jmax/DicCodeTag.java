package com.xdtech.web.freemark.tags.jmax;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.xdtech.core.init.InitCacheData;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.model.ComboBox;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class DicCodeTag implements TemplateDirectiveModel
{

	public void execute(Environment env, Map params,
						TemplateModel[] loopVars, TemplateDirectiveBody body)
																				throws TemplateException, IOException
	{
		// 真正开始处理输出内容
		Writer out = env.getOut();
		StringBuffer sb = new StringBuffer();
		Object key = params.get("key")==null?"":params.get("key");
		Object code = params.get("code");
//		sb.append("dfsdfdsfdsfsd");
//		sb.append(params.get("name"));
		List items = InitCacheData.dictionary.get(key.toString());
		List<ComboBox> comboBoxs = BeanUtils.copyListProperties(ComboBox.class, items);

		for (ComboBox comboBox : comboBoxs) {
			if (code!=null&&code.toString().equals(comboBox.getValue())) {
				sb.append(comboBox.getName());
				break;
			}
		}
		out.write(sb.toString());
		out.flush();
		
		
	}

}
