package com.xdtech.web.freemark.item;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface GridColumn {
	public String title() default "";

	public int width() default 80;
	//是否显示
	public boolean show() default true;
	
	public boolean editable() default false;
	//是否可以导出
	public boolean enableExport() default false;
	
	//以下几种格式化具有排斥关系，只能配置一种类型
	//自定义静态格式化
	public String[] formatter() default {};
	//数据字典格式化
	public String dictionaryCode() default "";
	//自定义Java动态执行代码格式化    具体包名#方法
	public String javaFormatter() default "";
	//自定义JS脚本数据转换 function 方法名
	public String jsFunctionFormatter() default "";
	
}
