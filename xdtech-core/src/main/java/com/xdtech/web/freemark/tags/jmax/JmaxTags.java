package com.xdtech.web.freemark.tags.jmax;

import freemarker.template.SimpleHash;

public class JmaxTags extends SimpleHash {
	private static final long serialVersionUID = 1L;

	public JmaxTags() {
        put("dc", new DicCodeTag());
    }
}
