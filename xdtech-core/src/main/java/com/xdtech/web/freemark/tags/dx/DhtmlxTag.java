/*
 * Project Name: xdtech-core
 * File Name: DhtmlxTag.java
 * Copyright: Copyright(C) 1985-2016 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.web.freemark.tags.dx;

import java.io.IOException;
import java.io.Writer;

import com.xdtech.web.freemark.tags.SecureTag;

import freemarker.core.Environment;

/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2016-7-22 上午11:18:42
 */
public abstract class DhtmlxTag extends SecureTag{
	protected void writeBody(Environment env,String content) throws IOException {
        if (env != null) {
        	// 真正开始处理输出内容
			Writer out = env.getOut();
			out.write(content);
			out.flush();
        }
    }
}
