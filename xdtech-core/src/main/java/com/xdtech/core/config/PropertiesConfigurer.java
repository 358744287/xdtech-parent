/*
 * Copyright 2013-2014 the original author or authors.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdtech.core.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * 
 * @author max.zheng
 * @create 2014-9-28下午9:08:19
 * @since 1.0
 * @see
 */
public class PropertiesConfigurer extends PropertyPlaceholderConfigurer {
	private static Log log = LogFactory.getLog(PropertiesConfigurer.class);
	private static Properties props = null;

	/**
	 * 根据applicationContext.xml启动时，加载配置信息
	 */
	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactoryToProcess,
			Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		PropertiesConfigurer.props = props;
	}

	/**
	 * @Description: 读取Spring配置文件里所有properties文件里的value值
	 * @Param: key 要设置属性的key
	 * @Return: value 要设置属性的值
	 */
	public static String getValue(String key) {
		return props.getProperty(key);
	}

	public static String sysInitModules() {
		String initModules = props.getProperty("system.initModules");
		return initModules;
	}
	
	public static boolean sysIsInitDataToDb() {
		String isInitData = props.getProperty("system.isInitDataToDb");
		return isInitData != null ? Boolean.valueOf(isInitData) : false;
	}
	
	public static void setAlreadyInitDataToDb() {
		props.setProperty("system.isInitDataToDb", "false");
	}
	
	public static boolean sysIsInitDataToCache() {
		String isInitData = props.getProperty("system.isInitDataToCache");
		return isInitData != null ? Boolean.valueOf(isInitData) : false;
	}
	
	/**
	 * 获取系统配置的工程目录
	 * @return
	 */
	public static String sysProjectPath() {
		return props.getProperty("project.code.path");
	}
	
	/**
	 * 开发者为dev，正式为rel，默认空的话就是正式
	 * @return
	 */
	public static String jmaxRunMode() {
		return props.getProperty("jmax.runMode");
	}
	
	/**
	 * JMax平台是否需要开始验证码校验
	 * @return
	 */
	public static String jmaxCheckCaptcha() {
		return props.getProperty("jmax.checkCaptcha");
	}
	
	/**
	 * 
	 * @author max.zheng
	 * @throws Exception 
	 * @throws  
	 * @create 2014-12-28下午10:01:58
	 * @modified by
	 */
	public static void changeAlreadyFlag(String key,String value) {
		try {
			File file = new File(PropertiesConfigurer.class.getResource("/").getPath()+"application.properties");
			BufferedReader bufferedReader = new BufferedReader(
					(new InputStreamReader(new FileInputStream(file), "utf-8")));
			StringBuffer stringBuffer = new StringBuffer();
			String line = null;
			boolean state = false;
			while ((line = bufferedReader.readLine()) != null) {
				if (line.contains(key)) {
					stringBuffer.append(key + "=" + value);
					state = true;
				} else {
					stringBuffer.append(line);
				}
				stringBuffer.append("\r\n");
			}
			OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(
					file), "utf-8");
			osw.write(stringBuffer.toString());
			osw.close();
		}catch (Exception e) {
			log.error("关闭初始化数据库参数异常",e);
		}
		
	}

}
