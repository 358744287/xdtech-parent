package com.xdtech.core.orm.hibernate.interceptor;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;

/**
 * 拦截通用数据底层数据通用字段更新信息
 * 
 * @author zhixiong
 * 
 */
public class PublicFieldInterceptor extends EmptyInterceptor {
	private static final long serialVersionUID = 1L;

	public boolean onFlushDirty(Object entity, Serializable id, Object[] state,
			Object[] previousState, String[] propertyNames, Type[] types)
			throws CallbackException {
		System.out.println("onFlushDirty");
		try {
			Date updateTime = new Date();
			for (int i = 0; i < propertyNames.length; i++) {
				if (propertyNames[i].equals("updateTime")) {
					state[i] = updateTime;
				}
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	public boolean onSave(Object entity, Serializable id, Object[] state,
			String[] propertyNames, Type[] types) throws CallbackException {
		System.out.println("onSave");
		try {
			Date updateTime = new Date();
			for (int i = 0; i < propertyNames.length; i++) {
				if (propertyNames[i].equals("createTime")) {
					state[i] = updateTime;
				}
			}
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

//	@Override
//	public String onPrepareStatement(String sql) {
//		// if(InterceptorUtil.isExcludePublicFieldTables(sql)){
//		// return super.onPrepareStatement(sql);
//		// }
//		System.out.println(sql);
//		// 更新
//		Pattern pattern = Pattern.compile("UPDATE\\s+",
//				Pattern.CASE_INSENSITIVE);
//
//		Matcher mathcer = pattern.matcher(sql);
//
//		if (mathcer.find()) {
//
//			// if(InterceptorUtil.isOnlyInsertPublicFieldTables(sql)){
//			//
//			// return super.onPrepareStatement(sql);
//			//
//			// }
//
//			// 当前用户ID
//			// String userId = LoginUserConstant.getLoginUser().getSysUserId();
//
//			// 程序ID
//			// String progId =
//			// StringUtils.substringBetween(request.getServletPath(),
//			// "/").toUpperCase();
//
//			// SQL语句后面增加共通字段
//			Pattern p = Pattern.compile("WHERE", Pattern.CASE_INSENSITIVE);
//
//			String[] sqls = p.split(sql);
//
//			StringBuffer sqlStrBuffer = new StringBuffer(sqls[0]);
//			// 判断是否已经有共通字段
//
//			// if(!InterceptorUtil.isHavePublicField(sql, "UPDATECOUNT", true)){
//			//
//			// sqlStrBuffer.append(",UPDATECOUNT=NVL(UPDATECOUNT,0)+1");
//			// }
//			//
//			// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEDATE", true)){
//			//
//			// sqlStrBuffer.append(",UPDATEDATE=SYSDATE");
//			//
//			// }
//			// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEUSERID",
//			// true)){
//			//
//			// sqlStrBuffer.append(",UPDATEUSERID=").append("'").append(userId).append("'");
//			//
//			// }
//			// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEPROGRAMID",
//			// true)){
//			//
//			// sqlStrBuffer.append(",UPDATEPROGRAMID=").append("'").append(progId).append("'");
//			//
//			// }
//			// if(sqls.length > 1 && !StringUtil.isEmpty(sqls[1])){
//			//
//			// sqlStrBuffer.append(" WHERE ");
//			//
//			// sqlStrBuffer.append(sqls[1]);
//			//
//			// }
//			// sql = sqlStrBuffer.toString();
//
//		} else {
//			// 插入
//			pattern = Pattern.compile("INSERT\\s+", Pattern.CASE_INSENSITIVE);
//
//			mathcer = pattern.matcher(sql);
//
//			if (mathcer.find()) {
//
//				// if(InterceptorUtil.isOnlyUpdatePublicFieldTables(sql)){
//				//
//				// return super.onPrepareStatement(sql);
//				//
//				// }
//
//				// // 当前用户ID
//				// String userId =
//				// LoginUserConstant.getLoginUser().getSysUserId();
//				//
//				// // 程序ID
//				// String progId =
//				// StringUtils.substringBetween(request.getServletPath(),
//				// "/").toUpperCase();
//
//				// SQL语句后面增加共通字段
//				Pattern p = Pattern.compile("values", Pattern.CASE_INSENSITIVE);
//
//				String[] sqls = p.split(sql);
//
//				StringBuffer sqlStrBuffer = new StringBuffer();
//
//				int bracesLastIndex0 = sqls[0].lastIndexOf(")");
//
//				int bracesLastIndex1 = sqls[1].lastIndexOf(")");
//
//				sqlStrBuffer.append(sqls[0].substring(0, bracesLastIndex0));
//
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATECOUNT",
//				// false)){
//				//
//				// sqlStrBuffer.append(",UPDATECOUNT");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "INSERTDATE",
//				// false)){
//				//
//				// sqlStrBuffer.append(",INSERTDATE");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "INSERTUSERID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",INSERTUSERID");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "INSERTPROGRAMID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",INSERTPROGRAMID");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEDATE",
//				// false)){
//				//
//				// sqlStrBuffer.append(",UPDATEDATE");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEUSERID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",UPDATEUSERID");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEPROGRAMID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",UPDATEPROGRAMID");
//				//
//				// }
//				sqlStrBuffer.append(") values");
//
//				sqlStrBuffer.append(sqls[1].substring(0, bracesLastIndex1));
//
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATECOUNT",
//				// false)){
//				//
//				// sqlStrBuffer.append(",0");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "INSERTDATE",
//				// false)){
//				//
//				// sqlStrBuffer.append(",SYSDATE");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "INSERTUSERID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",").append("'").append(userId).append("'");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "INSERTPROGRAMID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",").append("'").append(progId).append("'");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEDATE",
//				// false)){
//				//
//				// sqlStrBuffer.append(",SYSDATE");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEUSERID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",").append("'").append(userId).append("'");
//				//
//				// }
//				// if(!InterceptorUtil.isHavePublicField(sql, "UPDATEPROGRAMID",
//				// false)){
//				//
//				// sqlStrBuffer.append(",").append("'").append(progId).append("'");
//				//
//				// }
//				sqlStrBuffer.append(")");
//
//				// sql = sqlStrBuffer.toString();
//			}
//		}
//		return super.onPrepareStatement(sql);
//	}

}
