//package com.xdtech.core.orm.utils;
//
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import com.sun.tools.javac.code.Attribute.Constant;
//
//public class InterceptorUtils {
//	/**
//	 * 
//	 * 功能描述：判断是否没有共通字段
//	 * 
//	 * @author ： 创建日期 ：2014年1月6日 上午11:01:32
//	 * 
//	 * @param str
//	 *            SQL或Entity类名
//	 * @return
//	 * 
//	 *         修改历史 ：(修改人，修改时间，修改原因/内容)
//	 */
//	public static boolean isExcludePublicFieldTables(String str) {
//		if (Constant.EXCLUDE_PUBLIC_FIELD_TABLES == null
//				|| Constant.EXCLUDE_PUBLIC_FIELD_TABLES.length == 0) {
//			return false;
//		}
//		for (String exludeTable : Constant.EXCLUDE_PUBLIC_FIELD_TABLES) {
//			Pattern pattern = Pattern.compile(exludeTable,
//					Pattern.CASE_INSENSITIVE);
//			Matcher mathcer = pattern.matcher(str);
//			if (mathcer.find()) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * 
//	 * 功能描述：判断是否只有新增共通字段
//	 * 
//	 * @author ： 创建日期 ：2014年1月6日 上午11:01:32
//	 * 
//	 * @param str
//	 *            SQL或Entity类名
//	 * @return
//	 * 
//	 *         修改历史 ：(修改人，修改时间，修改原因/内容)
//	 */
//	public static boolean isOnlyInsertPublicFieldTables(String str) {
//		if (Constant.ONLY_INSERT_PUBLIC_FIELD_TABLES == null
//				|| Constant.ONLY_INSERT_PUBLIC_FIELD_TABLES.length == 0) {
//			return false;
//		}
//		for (String onlyTable : Constant.ONLY_INSERT_PUBLIC_FIELD_TABLES) {
//			Pattern pattern = Pattern.compile(onlyTable,
//					Pattern.CASE_INSENSITIVE);
//			Matcher mathcer = pattern.matcher(str);
//			if (mathcer.find()) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * 
//	 * 功能描述：判断是否只有修改共通字段
//	 * 
//	 * @author ： 创建日期 ：2014年1月6日 上午11:01:32
//	 * 
//	 * @param str
//	 *            SQL或Entity类名
//	 * @return
//	 * 
//	 *         修改历史 ：(修改人，修改时间，修改原因/内容)
//	 */
//	public static boolean isOnlyUpdatePublicFieldTables(String str) {
//		if (Constant.ONLY_UPDATE_PUBLIC_FIELD_TABLES == null
//				|| Constant.ONLY_UPDATE_PUBLIC_FIELD_TABLES.length == 0) {
//			return false;
//		}
//		for (String onlyTable : Constant.ONLY_UPDATE_PUBLIC_FIELD_TABLES) {
//			Pattern pattern = Pattern.compile(onlyTable,
//					Pattern.CASE_INSENSITIVE);
//			Matcher mathcer = pattern.matcher(str);
//			if (mathcer.find()) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * 
//	 * 功能描述：判断是否已经有该共通字段
//	 * 
//	 * @author ： 创建日期 ：2014年1月6日 上午11:01:32
//	 * 
//	 * @param sql
//	 *            SQL
//	 * @param field
//	 *            字段名称
//	 * @param isUpdate
//	 *            true-更新操作 false-插入操作
//	 * @return
//	 * 
//	 *         修改历史 ：(修改人，修改时间，修改原因/内容)
//	 */
//	public static boolean isHavePublicField(String sql, String field,
//			boolean isUpdate) {
//		String patternStr = field;
//		if (isUpdate) {
//			patternStr = patternStr + "\\s*=";
//		} else {
//			patternStr = ",?\\s*" + patternStr;
//		}
//		Pattern pattern = Pattern.compile(patternStr, Pattern.CASE_INSENSITIVE);
//		Matcher mathcer = pattern.matcher(sql);
//		if (mathcer.find()) {
//			return true;
//		}
//		return false;
//	}
//
//}
