package com.xdtech.core.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public abstract class BaseItem {
	private Map<String, String> queryFields = new HashMap<String, String>();
	
//	protected Map<String, String> queryConditions = new HashMap<String, String>();

	public Map<String, String> loadQueryFields() {
		return queryFields;
	}

	public void setQueryFields(Map<String, String> queryFields) {
		this.queryFields = queryFields;
	}
	
	public void addQuerys(String name,Object value) {
		if (value!=null&&queryFields!=null&&value.toString().trim()!="") {
			queryFields.put(name, value.toString());
		}
	}
	

//	public Map<String, String> getQueryConditions() {
//		return queryConditions;
//	}
//
//	public void setQueryConditions(Map<String, String> queryConditions) {
//		this.queryConditions = queryConditions;
//	}
	
	
	
	
}
