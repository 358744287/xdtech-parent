package com.xdtech.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class BaseModel {
	
	/**
	 * 记录创建时间不允许修改
	 */
	@Column(name="CREATE_TIME",updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createTime = new Date();
	@Column(name="CREATOR_ID",updatable=false)
	protected Long creatorId;
	@Column(name="CREATOR_CODE",updatable=false)
	protected String creatorCode;
	@Column(name="CREATOR_NAME",updatable=false)
	protected String creatorName;
	/**
	 * 记录修改时间，
	 */
	@Column(name="UPDATE_TIME")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date updateTime;
	@Column(name="UPDATED_ID")
	protected Long updatedId;
	@Column(name="UPDATED_CODE")
	protected String updatedCode;
	@Column(name="UPDATED_NAME")
	protected String updatedName;
	
	
	public BaseModel() {
		super();
		this.createTime = new Date();
		this.updateTime = new Date();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreatorCode() {
		return creatorCode;
	}

	public void setCreatorCode(String creatorCode) {
		this.creatorCode = creatorCode;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public Long getUpdatedId() {
		return updatedId;
	}

	public void setUpdatedId(Long updatedId) {
		this.updatedId = updatedId;
	}

	public String getUpdatedCode() {
		return updatedCode;
	}

	public void setUpdatedCode(String updatedCode) {
		this.updatedCode = updatedCode;
	}

	public String getUpdatedName() {
		return updatedName;
	}

	public void setUpdatedName(String updatedName) {
		this.updatedName = updatedName;
	}

	public void updateRecordMark(Long updateId,String updatedCode,String updatedName) {
		setUpdateTime(new Date());
		setUpdatedId(updateId);
		setUpdatedName(updatedName);
		setUpdatedCode(updatedCode);
	}
	public void createRecordMark(Long creatorId,String creatorCode,String creatorName) {
		setCreateTime(new Date());
		setCreatorId(creatorId);
		setCreatorName(creatorName);
		setCreatorCode(creatorCode);
	}
}
