package com.xdtech.common.service;

import java.util.List;
import java.util.Map;

import com.xdtech.core.model.BaseItem;
import com.xdtech.core.model.BaseModel;
import com.xdtech.web.model.Pagination;

public interface IBaseService<T extends BaseModel> {

	/**
	 * 保存实体
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:11:39
	 * @modified by
	 * @param entity
	 */
	public void save(final T entity);
	/**
	 * 保存实体集合
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:11:39
	 * @modified by
	 * @param entity
	 */
	public void saveAll(final List<T> entities);
	/**
	 * 删除实体
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:11:39
	 * @modified by
	 * @param entity
	 */
	public void delete(final T entity);
	/**
	 * 根据id删除实体
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:12:10
	 * @modified by
	 * @param id
	 */
	public void delete(final Long id);
	/**
	 * 根据id删除实体
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:12:19
	 * @modified by
	 * @param id
	 * @return
	 */
	public T get(final Long id);
	/**
	 * 获取所有数据
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:12:39
	 * @modified by
	 * @return
	 */
	public List<T> getAll();
	
	public Map<String, Object> loadPageAndCondition(Pagination pg,
			final Map<String, String> values);
	
	/**
	 * 通用的根据条件查询
	 * 
	 * @author max.zheng
	 * @create 2015-4-18下午12:12:58
	 * @modified by
	 * @param pg
	 * @param baseItem
	 * @param queryName
	 * @return
	 */
	public Map<String, Object> loadPageDataByConditions(Pagination pg,BaseItem baseItem,String queryName);

	
	
//	public void executeUpdateBySql(String sql);
//	
//	public void executeSql(String sql,Object... values);
	
//	public Map<String, Object> loadPageCondition(Pagination pg,
//			final BaseCondition condition);

}
