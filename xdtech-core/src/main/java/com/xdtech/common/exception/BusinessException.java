
package com.xdtech.common.exception;

/**
 * 业务异常定义
 * @author max 
 * @version v1.0.0
 * @since 2014-9-29 下午3:13:45
 */
public class BusinessException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public BusinessException()
	{
		super();
	}

	public BusinessException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public BusinessException(String message)
	{
		super(message);
	}

	public BusinessException(Throwable cause)
	{
		super(cause);
	}
	
	

}
