/*
 * Project Name: CZW_PRO
 * File Name: ExcelHelper.java
 * Copyright: Copyright(C) 1985-2014 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.common.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.xdtech.common.annotation.Excel;
import com.xdtech.core.init.InitCacheData;
import com.xdtech.core.orm.utils.BeanUtils;
import com.xdtech.web.freemark.item.GridColumn;
import com.xdtech.web.model.ComboBox;


/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author max.zheng
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2014-9-23 上午9:41:52
 */
public class ExcelHelper
{

	/**
	 * excel导出处理
	 * @author max.zheng
	 * @see 相关类或方法，不需要请删除此行
	 * @since 2014-9-23 上午9:42:38
	 * @param title
	 * @param pojoClass
	 * @param dataSet
	 * @param out
	 */
	public static void exportExcel(String title, Class pojoClass, Collection dataSet,
									OutputStream out)
	{
		//使用userModel模式实现的，当excel文档出现10万级别的大数据文件可能导致OOM内存溢出
		exportExcelInUserModel(title, pojoClass, dataSet, out);
		//使用eventModel实现，可以一边读一边处理，效率较高，但是实现复杂，暂时未实现
	}
	private static void exportExcelInUserModel(String title, Class pojoClass, Collection dataSet,
												OutputStream out)
	{
		try
		{
			// 首先检查数据看是否是正确的
//			if (dataSet == null || dataSet.size() == 0)
//			{
//				throw new Exception("导出数据为空！");
//			}
			if (title == null || out == null || pojoClass == null)
			{
				throw new Exception("传入参数不能为空！");
			}
			// 声明一个工作薄  
			Workbook workbook = new HSSFWorkbook();
			// 生成一个表格
			Sheet sheet = workbook.createSheet(title);

			// 标题
			List<String> exportFieldTitle = new ArrayList<String>();
			List<Integer> exportFieldWidth = new ArrayList<Integer>();
			// 拿到所有列名，以及导出的字段的get方法
			List<Method> methodObj = new ArrayList<Method>();
			Map<String, Method> convertMethod = new HashMap<String, Method>();
			// 得到所有字段
			Field fileds[] = pojoClass.getDeclaredFields();
			// 遍历整个filed
			for (int i = 0; i < fileds.length; i++)
			{
				Field field = fileds[i];
				Excel excel = field.getAnnotation(Excel.class);
				// 如果设置了annottion
				if (excel != null)
				{
					// 添加到标题
					exportFieldTitle.add(excel.exportName());
					//添加标题的列宽
					exportFieldWidth.add(excel.exportFieldWidth());
					// 添加到需要导出的字段的方法
					String fieldname = field.getName();
					//System.out.println(i+"列宽"+excel.exportName()+" "+excel.exportFieldWidth());
					StringBuffer getMethodName = new StringBuffer("get");
					getMethodName.append(fieldname.substring(0, 1)
							.toUpperCase());
					getMethodName.append(fieldname.substring(1));

					Method getMethod = pojoClass.getMethod(getMethodName.toString(),
							new Class[] {});

					methodObj.add(getMethod);
					if (excel.exportConvertSign() == 1)
					{
						StringBuffer getConvertMethodName = new StringBuffer("get");
						getConvertMethodName.append(fieldname.substring(0, 1)
								.toUpperCase());
						getConvertMethodName.append(fieldname.substring(1));
						getConvertMethodName.append("Convert");
						//System.out.println("convert: "+getConvertMethodName.toString());
						Method getConvertMethod = pojoClass.getMethod(getConvertMethodName.toString(),
								new Class[] {});
						convertMethod.put(getMethodName.toString(), getConvertMethod);
					}
				}
			}
			int index = 0;
			// 产生表格标题行
			Row row = sheet.createRow(index);
			for (int i = 0, exportFieldTitleSize = exportFieldTitle.size(); i < exportFieldTitleSize; i++)
			{
				Cell cell = row.createCell(i);
				//  cell.setCellStyle(style);
				RichTextString text = new HSSFRichTextString(
						exportFieldTitle.get(i));
				cell.setCellValue(text);
			}

			//设置每行的列宽
			for (int i = 0; i < exportFieldWidth.size(); i++)
			{
				//256=65280/255
				sheet.setColumnWidth(i, 256 * exportFieldWidth.get(i));
				//设置单元格格式为文本类型，默认常规情况会出现输入03导致变成3
				CellStyle css = workbook.createCellStyle();
				DataFormat  format = workbook.createDataFormat();
				css.setDataFormat(format.getFormat("@"));
				sheet.setDefaultColumnStyle(i,css);
			}
			Iterator its = dataSet.iterator();
			// 循环插入剩下的集合
			while (its.hasNext())
			{
				// 从第二行开始写，第一行是标题
				index++;
				row = sheet.createRow(index);
				Object t = its.next();
				for (int k = 0, methodObjSize = methodObj.size(); k < methodObjSize; k++)
				{
					Cell cell = row.createCell(k);
					Method getMethod = methodObj.get(k);
					Object value = null;
					if (convertMethod.containsKey(getMethod.getName()))
					{
						Method cm = convertMethod.get(getMethod.getName());
						value = cm.invoke(t, new Object[] {});
					}
					else
					{
						value = getMethod.invoke(t, new Object[] {});
					}
//					System.out.println(cell+"cell");
//					System.out.println(value+"value");
					cell.setCellValue(value==null?"":value.toString());
				}
			}
			

			workbook.write(out);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * excel导入处理
	 * @author max.zheng
	 * @see 相关类或方法，不需要请删除此行
	 * @since 2014-9-23 上午9:44:09
	 * @param file
	 * @param pojoClass
	 * @param pattern
	 * @return
	 */
	public static List importExcel(InputStream ins, Class pojoClass)
	{

		List dist = new ArrayList();
		try
		{
			// 得到目标目标类的所有的字段列表   
			Field filed[] = pojoClass.getDeclaredFields();
			// 将所有标有Annotation的字段，也就是允许导入数据的字段,放入到一个map中   
			Map<String, Method> fieldSetMap = new HashMap<String, Method>();

			Map<String, Method> fieldSetConvertMap = new HashMap<String, Method>();

			// 循环读取所有字段   
			for (int i = 0; i < filed.length; i++)
			{
				Field f = filed[i];

				// 得到单个字段上的Annotation   
				Excel excel = f.getAnnotation(Excel.class);

				// 如果标识了Annotationd的话   
				if (excel != null)
				{
					// 构造设置了Annotation的字段的Setter方法   
					String fieldname = f.getName();
					String setMethodName = "set"
											+ fieldname.substring(0, 1).toUpperCase()
											+ fieldname.substring(1);
					// 构造调用的method，   
					Method setMethod = pojoClass.getMethod(setMethodName,
							new Class[] {f.getType()});
					// 将这个method以Annotaion的名字为key来存入。 

					//对于重名将导致 覆盖失败，对于此处的限制需要
					fieldSetMap.put(excel.exportName(), setMethod);

					if (excel.importConvertSign() == 1)
					{
						StringBuffer setConvertMethodName = new StringBuffer("set");
						setConvertMethodName.append(fieldname.substring(0, 1)
								.toUpperCase());
						setConvertMethodName.append(fieldname.substring(1));
						setConvertMethodName.append("Convert");
						Method getConvertMethod = pojoClass.getMethod(setConvertMethodName.toString(),
								new Class[] {String.class});
						fieldSetConvertMap.put(excel.exportName(), getConvertMethod);
					}

				}
			}
			// 将传入的File构造为FileInputStream;   
//			FileInputStream in = new FileInputStream(file);
			// // 得到工作表   
			HSSFWorkbook book = new HSSFWorkbook(ins);
			// // 得到第一页   
			HSSFSheet sheet = book.getSheetAt(0);
			// // 得到第一面的所有行   
			Iterator<Row> row = sheet.rowIterator();
			// 得到第一行，也就是标题行   
			Row title = row.next();
			// 得到第一行的所有列   
			Iterator<Cell> cellTitle = title.cellIterator();
			// 将标题的文字内容放入到一个map中。   
			Map<Integer, String> titlemap = new HashMap<Integer, String>();
			// 从标题第一列开始   
			int i = 0;
			// 循环标题所有的列   
			while (cellTitle.hasNext())
			{
				Cell cell = cellTitle.next();
				String value = cell.getStringCellValue();
				titlemap.put(i, value);
				i = i + 1;
			}

			while (row.hasNext())
			{
				// 标题下的第一行   
				Row rown = row.next();
				// 行的所有列   
//				Iterator<Cell> cellbody = rown.cellIterator();
				// 得到传入类的实例   
				Object tObject = pojoClass.newInstance();

				int k = 0;
				//判断是否要加入，主要处理空行数据被添加情况
				boolean isAddObject = false;
				// 遍历一行的列 
				for (Integer cellIndex : titlemap.keySet()) {
					Cell cell = rown.getCell(cellIndex);
					if (cell==null) {
						continue;
					}
					// 这里得到此列的对应的标题   
					String titleString = (String) titlemap.get(cellIndex);
					// 如果这一列的标题和类中的某一列的Annotation相同，那么则调用此类的的set方法，进行设值   
					if (fieldSetMap.containsKey(titleString))
					{

						Method setMethod = (Method) fieldSetMap.get(titleString);
						//得到setter方法的参数   
						Type[] ts = setMethod.getGenericParameterTypes();
						//只要一个参数   
						String xclass = ts[0].toString();
						//判断参数类型   
//						System.out.println("类型: " + xclass);
						if (fieldSetConvertMap.containsKey(titleString))
						{
							fieldSetConvertMap.get(titleString).invoke(tObject,
									cell.getStringCellValue());
						}
						else
						{
							Object cellValue = getCellValue(cell);
							if(cellValue!=null) {
								isAddObject = true;
							}
							if (xclass.equals("class java.lang.String"))
							{
								setMethod.invoke(tObject, cellValue);
							}
							else if (xclass.equals("class java.util.Date"))
							{
								setMethod.invoke(tObject, cellValue);
							}
							else if (xclass.equals("class java.lang.Boolean"))
							{
								setMethod.invoke(tObject, cellValue);
							}
							else if (xclass.equals("class java.lang.Integer"))
							{
								setMethod.invoke(tObject, new Integer(cellValue.toString()));
							}
							else if (xclass.equals("class java.lang.Long"))
							{
								setMethod.invoke(tObject, new Long(cellValue.toString()));
							}
							else if (xclass.equals("class java.lang.Double"))
							{
								setMethod.invoke(tObject, new Double(cellValue.toString()));
							}
						}
					}
				}
//				while (cellbody.hasNext())
//				{
//					
//					// 下一列   
//					k = k + 1;
//				}
				if (isAddObject)
				{
					dist.add(tObject);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return dist;
	}
	/**
	 * 根据excel的单元格类型获取对应的值
	 * @author max.zheng
	 * @see 相关类或方法，不需要请删除此行
	 * @since 2014-9-24 上午9:20:06
	 * @param cell
	 * @return
	 */
	private static Object getCellValue(Cell cell)
	{
		Object value = null;
		if (cell != null)
		{
			switch (cell.getCellType())
			{
				case Cell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (DateUtil.isCellDateFormatted(cell))
					{
						value= cell.getDateCellValue();
					}
					else
					{
						value = new DecimalFormat("0").format(cell.getNumericCellValue());
					}
					break;
				case Cell.CELL_TYPE_FORMULA:
					// 导入时如果为公式生成的数据则无值
					if (!cell.getStringCellValue().equals(""))
					{
						value = cell.getStringCellValue();
					}
					else
					{
						value = cell.getNumericCellValue() + "";
					}
					break;
				case Cell.CELL_TYPE_BLANK:
					break;
				case Cell.CELL_TYPE_ERROR:
					value = "";
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					value = cell.getBooleanCellValue();
					break;
				default:
					value = "";
			}
		}
		return value;
	}
	
	/**
	 * 通过界面表格grid标记导出excel表格数据
	 * 
	 * @author max.zheng
	 * @create 2015-11-21上午12:14:03
	 * @modified by
	 * @param title
	 * @param pojoClass
	 * @param dataSet
	 * @param out
	 */
	public static void exportExcelByGrid(String title,String rowHeight, Class pojoClass, Collection dataSet,
									OutputStream out)
	{
		try
		{
			if (title == null || out == null || pojoClass == null)
			{
				throw new Exception("传入参数不能为空！");
			}
			// 声明一个工作薄  
			Workbook workbook = new HSSFWorkbook();
			// 生成一个表格
			Sheet sheet = workbook.createSheet(title);
			
			
			// 标题
//			List<String> exportFieldTitle = new ArrayList<String>();
//			List<Integer> exportFieldWidth = new ArrayList<Integer>();
//			// 拿到所有列名，以及导出的字段的get方法
//			List<Method> methodObj = new ArrayList<Method>();
//			Map<String, Method> convertMethod = new HashMap<String, Method>();
			List<ExportField> exportFields = new ArrayList<ExportField>();
			ExportField exportField = null;
			// 得到所有字段
			Field fileds[] = pojoClass.getDeclaredFields();
			// 遍历整个filed
			for (int i = 0; i < fileds.length; i++)
			{
				Field field = fileds[i];
				Excel excel = field.getAnnotation(Excel.class);
				// 如果设置了annottion
				if (excel != null)
				{
					// 添加到需要导出的字段的方法
					String fieldname = field.getName();
					//System.out.println(i+"列宽"+excel.exportName()+" "+excel.exportFieldWidth());
					StringBuffer getMethodName = new StringBuffer("get");
					getMethodName.append(fieldname.substring(0, 1)
							.toUpperCase());
					getMethodName.append(fieldname.substring(1));

					Method getMethod = pojoClass.getMethod(getMethodName.toString(),
							new Class[] {});
					exportField = new ExportField(excel.exportName(), excel.exportFieldWidth(), getMethod, excel.qrCode());
					exportFields.add(exportField);
				}
			}
			//行索引
			int index = 0;
			//列总数
			int columnFields = exportFields.size();
			// 产生表格标题行
			Row row = sheet.createRow(index);
			for (int i = 0;i < columnFields; i++)
			{
				Cell cell = row.createCell(i);
				//  cell.setCellStyle(style);
				RichTextString text = new HSSFRichTextString(exportFields.get(i).getFieldTitle());
				cell.setCellValue(text);
				//设置每行的列宽
				//256=65280/255
				sheet.setColumnWidth(i, 256 * exportFields.get(i).getFieldWidth());
				//设置单元格格式为文本类型，默认常规情况会出现输入03导致变成3
				CellStyle css = workbook.createCellStyle();
				DataFormat  format = workbook.createDataFormat();
				css.setDataFormat(format.getFormat("@"));
				sheet.setDefaultColumnStyle(i,css);
			}
			
			HSSFPatriarch patri = null;
			HSSFClientAnchor anchor = null;
			Iterator its = dataSet.iterator();
			// 循环插入剩下的集合
			while (its.hasNext())
			{
				// 从第二行开始写，第一行是标题
				index++;
				row = sheet.createRow(index);
				if (StringUtils.isNotBlank(rowHeight)) {
					row.setHeightInPoints(Float.valueOf(rowHeight));
				}
				Object t = its.next();
				for (int k = 0; k < columnFields; k++)
				{
					Cell cell = row.createCell(k);
					Method getMethod = exportFields.get(k).getFieldMethod();
					Object value = getMethod.invoke(t, new Object[] {});
//					System.out.println(cell+"cell");
//					System.out.println(value+"value");
					if (!exportFields.get(k).isQrCode()) {
						cell.setCellValue(value==null?"":value.toString());
					}else {
						if (value!=null&&StringUtils.isNotBlank(value+"")) {
							ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();     
							BufferedImage bufferImg = ImageIO.read(new File(value+""));     
				            ImageIO.write(bufferImg, "png", byteArrayOut);  
							patri = (HSSFPatriarch) sheet
									.createDrawingPatriarch();
							anchor = new HSSFClientAnchor(0, 0, 0, 0,
									(short) cell.getColumnIndex(), index, (short) (cell.getColumnIndex() + 1),
									index + 1);
							patri.createPicture(anchor, workbook.addPicture(
									byteArrayOut.toByteArray(),
									HSSFWorkbook.PICTURE_TYPE_PNG));

							bufferImg.flush();
						}
						
					}
					
				}
			}
			

			workbook.write(out);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 通过界面表格grid标记导出excel表格数据
	 * 
	 * @author max.zheng
	 * @create 2015-11-21上午12:14:03
	 * @modified by
	 * @param title
	 * @param pojoClass
	 * @param dataSet
	 * @param out
	 */
	public static void exportByGrid(String title,String rowHeight, Class pojoClass, Collection dataSet,
									OutputStream out)
	{
		try
		{
			if (title == null || out == null || pojoClass == null)
			{
				throw new Exception("传入参数不能为空！");
			}
			// 声明一个工作薄  
			Workbook workbook = new HSSFWorkbook();
			// 生成一个表格
			Sheet sheet = workbook.createSheet(title);

			List<ExportField> exportFields = new ArrayList<ExportField>();
			ExportField exportField = null;
			// 得到所有字段
			Field fileds[] = pojoClass.getDeclaredFields();
			// 遍历整个filed
			for (int i = 0; i < fileds.length; i++)
			{
				Field field = fileds[i];
				GridColumn gridColumn = field.getAnnotation(GridColumn.class);
				// 如果设置了annottion
				if (gridColumn != null&&gridColumn.enableExport())
				{
					// 添加到需要导出的字段的方法
					String fieldname = field.getName();
					//System.out.println(i+"列宽"+excel.exportName()+" "+excel.exportFieldWidth());
					StringBuffer getMethodName = new StringBuffer("get");
					getMethodName.append(fieldname.substring(0, 1)
							.toUpperCase());
					getMethodName.append(fieldname.substring(1));

					Method getMethod = pojoClass.getMethod(getMethodName.toString(),
							new Class[] {});
					exportField = new ExportField(gridColumn.title(), gridColumn.width()/6, getMethod, false,gridColumn.dictionaryCode());
					exportFields.add(exportField);
				}
			}
			//行索引
			int index = 0;
			//列总数
			int columnFields = exportFields.size();
			// 产生表格标题行
			Row row = sheet.createRow(index);
			for (int i = 0;i < columnFields; i++)
			{
				Cell cell = row.createCell(i);
				//  cell.setCellStyle(style);
				RichTextString text = new HSSFRichTextString(exportFields.get(i).getFieldTitle());
				cell.setCellValue(text);
				//设置每行的列宽
				//256=65280/255
				sheet.setColumnWidth(i, 256 * exportFields.get(i).getFieldWidth());
				//设置单元格格式为文本类型，默认常规情况会出现输入03导致变成3
				CellStyle css = workbook.createCellStyle();
				DataFormat  format = workbook.createDataFormat();
				css.setDataFormat(format.getFormat("@"));
				sheet.setDefaultColumnStyle(i,css);
			}
			
			HSSFPatriarch patri = null;
			HSSFClientAnchor anchor = null;
			Iterator its = dataSet.iterator();
			// 循环插入剩下的集合
			while (its.hasNext())
			{
				// 从第二行开始写，第一行是标题
				index++;
				row = sheet.createRow(index);
				if (StringUtils.isNotBlank(rowHeight)) {
					row.setHeightInPoints(Float.valueOf(rowHeight));
				}
				Object t = its.next();
				for (int k = 0; k < columnFields; k++)
				{
					Cell cell = row.createCell(k);
					Method getMethod = exportFields.get(k).getFieldMethod();
					Object value = getMethod.invoke(t, new Object[] {});
//					System.out.println(cell+"cell");
//					System.out.println(value+"value");
					if (!exportFields.get(k).isQrCode()) {
						cell.setCellValue(value==null?"":value.toString());
						if (EmptyUtil.isNotEmpty(exportFields.get(k).getConvertDictionaryCode())) {
							//转换字典存在时候
							List items = InitCacheData.dictionary.get(exportFields.get(k).getConvertDictionaryCode());
							List<ComboBox> comboBoxs = BeanUtils.copyListProperties(ComboBox.class, items);
							for (int i = 0; i < comboBoxs.size(); i++)
							{
								ComboBox cb = comboBoxs.get(i);
								if (cb.getValue().equals(value)) {
									cell.setCellValue(cb.getName());
									break;
								}
								
							}
						}
					}else {
						if (value!=null&&StringUtils.isNotBlank(value+"")) {
							ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();     
							BufferedImage bufferImg = ImageIO.read(new File(value+""));     
				            ImageIO.write(bufferImg, "png", byteArrayOut);  
							patri = (HSSFPatriarch) sheet
									.createDrawingPatriarch();
							anchor = new HSSFClientAnchor(0, 0, 0, 0,
									(short) cell.getColumnIndex(), index, (short) (cell.getColumnIndex() + 1),
									index + 1);
							patri.createPicture(anchor, workbook.addPicture(
									byteArrayOut.toByteArray(),
									HSSFWorkbook.PICTURE_TYPE_PNG));

							bufferImg.flush();
						}
						
					}
					
				}
			}
			

			workbook.write(out);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

class ExportField {
	private String fieldTitle;
	private Integer fieldWidth;
	private Method fieldMethod;
	private boolean qrCode;
	
	private String convertDictionaryCode;
	
	public ExportField() {
		super();
	}
	
	
	public ExportField(String fieldTitle, Integer fieldWidth,
			Method fieldMethod, boolean qrCode) {
		super();
		this.fieldTitle = fieldTitle;
		this.fieldWidth = fieldWidth;
		this.fieldMethod = fieldMethod;
		this.qrCode = qrCode;
	}


	public ExportField(String fieldTitle, Integer fieldWidth,
			Method fieldMethod, boolean qrCode, String convertDictionaryCode) {
		super();
		this.fieldTitle = fieldTitle;
		this.fieldWidth = fieldWidth;
		this.fieldMethod = fieldMethod;
		this.qrCode = qrCode;
		this.convertDictionaryCode = convertDictionaryCode;
	}


	public String getConvertDictionaryCode() {
		return convertDictionaryCode;
	}


	public void setConvertDictionaryCode(String convertDictionaryCode) {
		this.convertDictionaryCode = convertDictionaryCode;
	}


	public String getFieldTitle() {
		return fieldTitle;
	}
	public void setFieldTitle(String fieldTitle) {
		this.fieldTitle = fieldTitle;
	}
	public Integer getFieldWidth() {
		return fieldWidth;
	}
	public void setFieldWidth(Integer fieldWidth) {
		this.fieldWidth = fieldWidth;
	}
	public Method getFieldMethod() {
		return fieldMethod;
	}
	public void setFieldMethod(Method fieldMethod) {
		this.fieldMethod = fieldMethod;
	}
	public boolean isQrCode() {
		return qrCode;
	}
	public void setQrCode(boolean qrCode) {
		this.qrCode = qrCode;
	}
	
	
	
	
}