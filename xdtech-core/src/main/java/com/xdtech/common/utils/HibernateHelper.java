/*
 * Project Name: xdtech-core
 * File Name: HibernateHelper.java
 * Copyright: Copyright(C) 1985-2015 ZKTeco Inc. All rights reserved.
 */
package com.xdtech.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.JoinedSubclassEntityPersister;
import org.hibernate.persister.entity.SingleTableEntityPersister;

/**
 * TODO 一句话功能简述，请确保和下面的block tags之间保留一行空行
 * <p>
 * TODO 功能详细描述，若不需要请连同上面的p标签一起删除
 * 
 * @author <a href="max.zheng@zkteco.com">郑志雄</>
 * @version TODO 添加版本
 * @see 相关类或方法，不需要请删除此行
 * @since 2015-3-18 下午4:00:22
 */
public class HibernateHelper
{
	public static List<String> getTableNames() {
		SessionFactory sessionFactory = ApplicationContextUtil.getContext().getBean(SessionFactory.class);
		Map<String, ClassMetadata> map = sessionFactory.getAllClassMetadata();
		List<String> tableNames = new ArrayList<String>();
		for (Entry<String, ClassMetadata> entry : map.entrySet())
		{
			String key = entry.getKey();
			ClassMetadata value = entry.getValue();
			String tableName = "";
			if (value instanceof SingleTableEntityPersister)
			{
				tableName = ((SingleTableEntityPersister)value).getTableName();
			}
			else if(value instanceof JoinedSubclassEntityPersister)
			{
				tableName = ((JoinedSubclassEntityPersister)value).getTableName();
			}
			
			tableNames.add(tableName);
		}
		return tableNames;
	}
	
	 /**
     * 获得列名
     * 
     * @param clazz 映射到数据库的po类
     * @param icol 第几列
     * @return String
     */
    public static String getColumnName(Class clazz, int icol) {
        // return getPersistentClass( clazz
        // ).getTable().getPrimaryKey().getColumn( 0 ).getName(); //獲取主鍵名
        return "";
    }
 
    /**
     * 获得所有列名
     * 
     * @param clazz 映射到数据库的po类
     * @return List<String> 列名
     */
    public static List<String> getColumnNames(String tableName) {
    	SessionFactory sessionFactory = ApplicationContextUtil.getContext().getBean(SessionFactory.class);
		Map<String, ClassMetadata> map = sessionFactory.getAllClassMetadata();
		List<String> columns = new ArrayList<String>();
		for (Entry<String, ClassMetadata> entry : map.entrySet())
		{
			String key = entry.getKey();
			ClassMetadata value = entry.getValue();
			String tempName = "";
			String[] tempColumns = null;
			if (value instanceof SingleTableEntityPersister)
			{
				tempName = ((SingleTableEntityPersister)value).getTableName();
				tempColumns = ((SingleTableEntityPersister)value).getSubclassColumnReaderTemplateClosure();
			}
			else if(value instanceof JoinedSubclassEntityPersister)
			{
				tempName = ((JoinedSubclassEntityPersister)value).getTableName();
				tempColumns = ((JoinedSubclassEntityPersister)value).getSubclassColumnReaderTemplateClosure();
			}
			if (tableName.equals(tempName))
			{
				//$PlaceHolder$.CREATE_TIME
				for (String temp:tempColumns) {
					columns.add(temp.replace("$PlaceHolder$.", ""));
				}
			}
		}
		return columns;
    }
}
