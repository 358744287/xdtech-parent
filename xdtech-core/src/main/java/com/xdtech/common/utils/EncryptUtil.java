package com.xdtech.common.utils;

import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 对密码进行加密和验证的类
 */
public class EncryptUtil {

	// 十六进制下数字到字符的映射数组
	private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

	/** * 把inputString加密 */
	public static String encodePassword(String inputString) {
		return encodeByMD5(inputString);
	}

	/**
	 * 验证输入的密码是否正确
	 * 
	 * @param password
	 *            加密后的密码
	 * @param inputString
	 *            输入的字符串
	 * @return 验证结果，TRUE:正确 FALSE:错误
	 */
	public static boolean validatePassword(String password, String inputString) {
		if (password.equals(encodeByMD5(inputString))) {
			return true;
		} else {
			return false;
		}
	}

	/** 对字符串进行MD5加密 */
	public static String encodeByMD5(String originString) {
		if (originString != null) {
			try {
				// 创建具有指定算法名称的信息摘要
				MessageDigest md = MessageDigest.getInstance("MD5");
				// 使用指定的字节数组对摘要进行最后更新，然后完成摘要计算
				byte[] results = md.digest(originString.getBytes());
				// 将得到的字节数组变成字符串返回
				String resultString = byteArrayToHexString(results);
				return resultString.toUpperCase();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 转换字节数组为十六进制字符串
	 * 
	 * @param 字节数组
	 * @return 十六进制字符串
	 */
	private static String byteArrayToHexString(byte[] b) {
		StringBuffer resultSb = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			resultSb.append(byteToHexString(b[i]));
		}
		return resultSb.toString();
	}

	/** 将一个字节转化成十六进制形式的字符串 */
	private static String byteToHexString(byte b) {
		int n = b;
		if (n < 0)
			n = 256 + n;
		int d1 = n / 16;
		int d2 = n % 16;
		return hexDigits[d1] + hexDigits[d2];
	}

	public static final String ALGORITHM = "DES";
	public static final String KEY = "www.jmax4you.com";

	/**
	 * DES加密
	 * 
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String data) {
		if (data == null || data.trim().length() == 0) {
			throw new IllegalArgumentException("加密内容不能为空");
		}
		String desStr = "";
		try {
			Key k = toKey((new BASE64Decoder()).decodeBuffer(KEY));
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, k);
			desStr = (new BASE64Encoder()).encodeBuffer(cipher.doFinal(data
					.getBytes()));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return desStr;
	}

	/**
	 * 转换密钥<br>
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	private static Key toKey(byte[] key) throws Exception {
		DESKeySpec dks = new DESKeySpec(key);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
		SecretKey secretKey = keyFactory.generateSecret(dks);
		return secretKey;
	}

	/**
	 * DES解密
	 * 
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String encryptData) {

		try {
			if (encryptData == null || encryptData.trim().length() == 0) {
				throw new IllegalArgumentException("加密参数不能为空");
			}
			String desStr = "";
			Key k = toKey((new BASE64Decoder()).decodeBuffer(KEY));

			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, k);

			byte decryptByet[] = cipher.doFinal((new BASE64Decoder())
					.decodeBuffer(encryptData));

			desStr = new String(decryptByet);

			return desStr;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	public static void main(String[] args) {
		// String pwd1="123";
		// String pwd2="";
		// CipherUtil cipher = new CipherUtil();
		// System.out.println("未加密的密码:"+pwd1);
		// //将123加密
		// pwd2 = cipher.generatePassword(pwd1);
		// System.out.println("加密后的密码:"+pwd2);
		// System.out.println(cipher.generatePassword("adminaa").length());
		//
		// System.out.print("验证密码是否下确:");
		// if(cipher.validatePassword(pwd2, pwd1)) {
		// System.out.println("正确");
		// }
		// else {
		// System.out.println("错误");
		// }
//		System.out.println(encodePassword("admin"));
		System.out.println(EncryptUtil.encodePassword("111111"));
//		System.out.println(encrypt("0-101-101"));
//		System.out.println("---");
//		System.out.println(decrypt("gHqgSll0O8POBKZp41rJpg=="));
	}

}
