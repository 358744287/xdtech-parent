package com.xdtech.common.utils;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @author zzx
 *
 */
public class ApplicationContextUtil implements ApplicationContextAware {

	private static ApplicationContext context;
	
	private static ServletContext application;

	
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		ApplicationContextUtil.context = context;
	}

	public static ApplicationContext getContext() {
		return context;
	}

	public static ServletContext getApplication() {
		return application;
	}

	public static void setApplication(ServletContext application) {
		ApplicationContextUtil.application = application;
	}
	/**
	 * 获取容器发布项目路径
	 * @return
	 * @throws Exception
	 */
	public static String getWebappUrl() throws Exception {
		//E:\tomcat\webapps\ROOT
		String projectPath =  ApplicationContextUtil.getApplication().getRealPath("");
		int endIndex = projectPath.lastIndexOf("\\");
		projectPath = projectPath.substring(0, endIndex+1);
		String realpath = java.net.URLDecoder.decode(projectPath, "UTF-8");
		return realpath;
	}
	
	/**
	 * 获取web项目发布的根路径
	 * @return 
	 * @throws Exception
	 */
	public static String getWebProUrl() throws Exception {
		//E:\tomcat\webapps\ROOT
		String projectPath =  ApplicationContextUtil.getApplication().getRealPath("");
//		int endIndex = projectPath.lastIndexOf("\\");
//		projectPath = projectPath.substring(0, endIndex+1);
		String realpath = java.net.URLDecoder.decode(projectPath, "UTF-8");
		return realpath;
	}
}
